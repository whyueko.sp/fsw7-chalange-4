class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
  }

  async init() {
    await this.load();
    this.clearButton.addEventListener("click", (e) => {
      e.preventDefault();
      let child = this.carContainerElement.firstElementChild;

      while (child) {
          child.remove();
          child = this.carContainerElement.firstElementChild;
      }
  })

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
  }

  run = () => {
    Car.list.forEach((car) => {
      const node = document.createElement("div");
      node.className = ("col-12", "col-md-6", "col-lg-4");
      node.innerHTML = car.render();
      this.carContainerElement.appendChild(node);
    });
  };

  async load() {
    const container = this.carContainerElement
    const submitButton = document.querySelector('#load-btn');
    const input = await document.querySelector('#capacity');
    const date = await document.querySelector('#tanggal');

    submitButton.addEventListener('click', async function() {
      container.innerHTML = "";
      const capacity = parseInt(input.value);
      const inputDate = date.value;
      const cars = await Binar.listCars((item) => {
        return item.capacity == capacity &&
        item.availableAt.toISOString().split ( "T" ) <= inputDate;
      });
      Car.init(cars);
    })

  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
