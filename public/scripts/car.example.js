class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
    <div class="card mb-3" style="max-width: 540px;">
      <div class="card-body mx-auto" style="width: 350px;">
        <img src="${this.image}" alt="${this.manufacture}" class="card-img-top mb-2" alt="Responsive image" style="height:300px;>
        <p class="card-title">${this.type} / ${this.model}</p>
        <p class="text-success">Rp ${this.rentPerDay.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')} / hari</p>
        <p>${this.description}</p>
        <p><i class="fa-solid fa-user"></i> ${this.capacity} orang</p>
        <p><i class="fas fa-cog"></i> ${this.transmission}</p>
        <p><i class="fa-solid fa-calendar"></i> Tahun ${this.year}</p>
        <p>available at: <b>${this.availableAt}</b></p>
        <a class="text-white btn btn-success" id="load-btn"> Pilih Mobil </a>
      </div>
    </div>
    `;
  }
}
